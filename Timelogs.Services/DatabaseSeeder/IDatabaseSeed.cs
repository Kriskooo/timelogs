﻿namespace Timelogs.Services.DatabaseSeeder
{
    public interface IDatabaseSeed
    {
        Task InitializeDatabase();
    }
}