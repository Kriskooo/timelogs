﻿using Microsoft.EntityFrameworkCore;
using Timelogs.Models.Models;
using Timelogs.Services.Interfaces;

namespace Timelogs.Database.SeedModels
{
    public class UserSeed : IModelSeeder
    {
        private readonly string[] FirstNames = { "John", "Gringo", "Mark", "Lisa", "Maria", "Sonya", "Philip", "Jose", "Lorenzo", "George", "Justin" };
        private readonly string[] LastNames = { "Johnson", "Lamas", "Jackson", "Brown", "Mason", "Rodriguez", "Roberts", "Thomas", "Rose", "McDonalds" };
        private readonly string[] EmailDomains = { "hotmail.com", "gmail.com", "live.com" };
        public async Task SeedAsync(AppDbContext _dbContext)
        {
            if (await _dbContext.Users.AnyAsync())
            {
                return;
            }

            var random = new Random();

            var users = new List<User>();

            for (int i = 0; i < 100; i++)
            {
                var firstName = FirstNames[random.Next(FirstNames.Length)];
                var lastName = LastNames[random.Next(LastNames.Length)];
                var emailDomain = EmailDomains[random.Next(EmailDomains.Length)];

                var user = new User
                {
                    FirstName = firstName,
                    LastName = lastName,
                    Email = $"{firstName.ToLower()}{lastName.ToLower()}@{emailDomain}"
                };

                users.Add(user);
            }
            await _dbContext.Users.AddRangeAsync(users);
        }
    }
}
