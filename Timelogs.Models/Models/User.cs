﻿using System.ComponentModel.DataAnnotations;
using Timelogs.Models.Models.Abstraction;

namespace Timelogs.Models.Models
{
    public class User : AbstractModel
    {
        public User()
        {
            this.TimeLogs = new List<TimeLog>();
        }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Email { get; set; }
        public ICollection<TimeLog> TimeLogs { get; set; }
    }
}
